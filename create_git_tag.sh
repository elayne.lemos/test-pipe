#!/bin/bash

RELEASE_TAG=''

echo -e "Commit short SHA: $CI_COMMIT_SHORT_SHA\nCommit title: $CI_COMMIT_TITLE"
echo 'Checking if the new tag already exists...'

TAG_ALREADY_CREATED="$(git show-ref --tags | grep $CI_COMMIT_SHORT_SHA)" || true
if [[ $TAG_ALREADY_CREATED ]]; then
    echo "Tag already created, reference: $TAG_ALREADY_CREATED"
    exit 0
fi

if [[ "$CI_COMMIT_TITLE" =~ (.*release.*)|(.*Release.*) ]]; then
    echo "Defining release tag from '$CI_COMMIT_TITLE'";
    RELEASE_TAG="${CI_COMMIT_TITLE#*/}"
    RELEASE_TAG="${RELEASE_TAG%\' *}"
else
    echo 'Defining hotfix tag...';
    RELEASE_TAG="$(git describe --tags --abbrev=0)";
    echo "Reference tag: $RELEASE_TAG"
    LAST_DIGIT="$(echo "$RELEASE_TAG" | cut -d '.' -f 3)";
    RELEASE_TAG="${RELEASE_TAG%.*}.$(($LAST_DIGIT + 1))";
fi

MERGE_REQUEST_IID="${CI_COMMIT_DESCRIPTION#*\!}"
BASE_URL="https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}"
ROUTE="merge_requests/${MERGE_REQUEST_IID}"

TAG_NOTES=$(curl "${BASE_URL}/${ROUTE}" --header "PRIVATE-TOKEN:  ${GITLAB_API_TOKEN}" \
    | python3 -c "import sys, json; print(json.load(sys.stdin, strict=False)['description'])")

curl_params=(
    --header "PRIVATE-TOKEN: ${GITLAB_API_TOKEN}"
    -d "tag_name=${RELEASE_TAG}"
    -d "description=${TAG_NOTES}"
    -d "ref=${CI_COMMIT_SHA}"
    --request POST
)
curl "${BASE_URL}/releases" "${curl_params[@]}"; echo
